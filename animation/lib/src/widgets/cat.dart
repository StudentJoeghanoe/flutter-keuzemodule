import 'package:flutter/material.dart';

class Cat extends StatelessWidget {
  const Cat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
        'https://i.pinimg.com/originals/ae/1e/d9/ae1ed91ec5d4db8efd9d4a9d10754fdd.png');
  }
}
