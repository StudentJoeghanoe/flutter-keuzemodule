import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news/src/blocs/stories_provider.dart';
import 'package:news/src/blocs/comments_provider.dart';
import 'package:news/src/screens/news_detail.dart';
import './screens/news_list.dart';
import './screens/news_detail.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CommentsProvider(
      child: StoriesProvider(
        child: MaterialApp(
          title: 'News App',
          onGenerateRoute: routes,
        ),
      ),
    );
  }

  Route routes(RouteSettings settings) {
    if (settings.name == '/') {
      return MaterialPageRoute(
        builder: (context) {
          final storiesBloc = StoriesProvider.of(context);

          storiesBloc.fetchTopIds();
          return const NewsList();
        },
      );
    }
    if (settings.name!.startsWith('/news')) {
      return MaterialPageRoute(
        builder: (context) {
          final commentsBloc = CommentsProvider.of(context);
          final int itemId =
              int.parse(settings.name!.replaceFirst('/news/', ''));

          commentsBloc.fetchItemWithComments(itemId);
          return NewsDetail(itemId: itemId);
        },
      );
    }

    return MaterialPageRoute(
      builder: (context) {
        return const Text('Error, page not availible');
      },
    );
  }
}
