import 'dart:async';
import 'package:flutter/material.dart';
import 'package:news/src/widgets/loading_container.dart';
import '../models/item_model.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/parser.dart' as htmlparser;
import 'package:html/dom.dart' as dom;

class Comment extends StatelessWidget {
  final int itemId;
  final Map<int, Future<ItemModel>> itemMap;
  final int depth;

  const Comment(
      {Key? key,
      required this.itemId,
      required this.itemMap,
      required this.depth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: itemMap[itemId],
      builder: (context, AsyncSnapshot<ItemModel> snapshot) {
        if (!snapshot.hasData) {
          return const LoadingContainer();
        }

        final item = snapshot.data;
        double leftPadding = 16.0 + (depth * 8.0);

        if (leftPadding >= 64.0) {
          leftPadding = 64.0;
        }

        final children = <Widget>[
          ListTile(
            contentPadding: EdgeInsets.only(
                top: 16.0, right: 16.0, bottom: 8.0, left: leftPadding),
            title: item!.by == "" ? null : buildText(item),
            subtitle: item.by == "" ? const Text("deleted") : Text(item.by),
          ),
          const Divider(
            height: 1.0,
          )
        ];

        for (var commentId in item.kids) {
          children.add(
            Comment(
              itemId: commentId,
              itemMap: itemMap,
              depth: depth + 1,
            ),
          );
        }

        return Column(
          children: children,
        );
      },
    );
  }

  Widget buildText(ItemModel item) {
    return Html(
      data: item.text,
      onLinkTap: (String? url, RenderContext context,
          Map<String, String> attributes, dom.Element? element) {
        //open URL in webview, or launch URL in browser, or any other logic here
      },
    );
  }
}
