import 'package:flutter/material.dart';

class LoadingContainer extends StatelessWidget {
  const LoadingContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          contentPadding: const EdgeInsets.all(16.0),
          title: buildContainer(),
          subtitle: buildContainer(),
        ),
        const Divider(
          height: 1.0,
        ),
      ],
    );
  }

  Widget buildContainer() {
    return Container(
      color: Colors.grey[300],
      height: 24.0,
      width: 150.0,
      margin: const EdgeInsets.only(top: 4.0, bottom: 4.0),
    );
  }
}
