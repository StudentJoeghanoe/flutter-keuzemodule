void main() {
  var deck = Deck();
  print(deck.cards);

  deck.shuffle();

  print(deck.deal(5));
  print(deck.cards);

  deck.removeCard('Diamonds', 'Ace');
  print(deck.cards);
}

class Deck {
  List<Card> cards = [];

  Deck() {
    List<String> ranks = [
      'Ace',
      'Two',
      'Three',
      'Four',
      'Five',
      'Six',
      'Seven',
      'Eight',
      'Nine',
      'Ten',
      'Jack',
      'Queen',
      'King'
    ];

    List<String> suits = ['Diamonds', 'Hearts', 'Clubs', 'Spades'];

    for (String mySuit in suits) {
      for (String rank in ranks) {
        Card card = Card(suit: mySuit, rank: rank);
        cards.add(card);
      }
    }
  }

  @override
  String toString() => cards.toString();

  Future<void> shuffle() async => cards.shuffle();

  Future<Iterable<Card>> cardsWithSuit(String suit) async =>
      cards.where((card) => card.suit == suit);

  Future<List<Card>> deal(int handSize) async {
    var hand = cards.sublist(0, handSize);
    cards = cards.sublist(handSize);

    return hand;
  }

  void removeCard(String suit, String rank) =>
      cards.removeWhere((card) => (card.suit == suit) && (card.rank == rank));
}

class Card {
  String suit;
  String rank;

  Card({this.suit = '', this.rank = ''});

  @override
  String toString() => '$suit $rank';
}
