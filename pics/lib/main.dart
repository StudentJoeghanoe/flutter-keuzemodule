// I need to import a helper library from flutter
import 'package:flutter/material.dart';
import 'src/app.dart';

// Define a main
void main() {
  const app = App();

  runApp(app);
}
