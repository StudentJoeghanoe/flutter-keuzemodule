import 'package:flutter/material.dart';
import '../models/image_model.dart';

class ImageList extends StatelessWidget {
  final List<ImageModel> images;

  const ImageList(this.images, {Key? key}) : super(key: key);

  @override
  Widget build(context) {
    return ListView.builder(
      itemCount: images.length,
      itemBuilder: (context, int index) {
        return buildImage(images[index]);
      },
    );
  }
}

Widget buildImage(ImageModel image) {
  return Container(
    decoration:
        BoxDecoration(border: Border.all(color: Colors.black, width: 1)),
    padding: const EdgeInsets.all(20.0),
    margin: const EdgeInsets.all(20.0),
    child: Column(
      children: [
        Padding(
          child: Image.network(image.url),
          padding: const EdgeInsets.only(bottom: 16.0),
        ),
        Text(image.title)
      ],
    ),
  );
}
