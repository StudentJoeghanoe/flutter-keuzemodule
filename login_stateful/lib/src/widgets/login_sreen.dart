import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../mixins/validation_mixin.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> with ValidationMixin {
  final formKey = GlobalKey<FormState>();

  String email = '';
  String password = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 32.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: const EdgeInsets.only(top: 24.0)),
            passwordField(),
            Container(margin: const EdgeInsets.only(top: 24.0)),
            submitbutton(),
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      decoration: const InputDecoration(
        label: Text('Email'),
        hintText: 'you@example.com',
      ),
      keyboardType: TextInputType.emailAddress,
      validator: validateEmail,
      onSaved: (String? value) {
        if (value != null) {
          setState(() {
            email = value;
          });
        }
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        label: Text('Password'),
        hintText: 'password123',
      ),
      validator: validatePassword,
      onSaved: (String? value) {
        if (value != null) {
          setState(() {
            password = value;
          });
        }
      },
    );
  }

  Widget submitbutton() {
    final ButtonStyle style = ElevatedButton.styleFrom(
      // onPrimary: Colors.black,
      primary: Colors.black,
    );

    return ElevatedButton(
      child: const Text('Submit'),
      style: style,
      onPressed: () {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          // ignore: avoid_print
          print('email is $email and password is $password');
        }
      },
    );
  }
}
