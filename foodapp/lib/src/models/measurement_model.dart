class MeasurementModel {
  final int amount;
  final String unitLong;
  final String unitShort;

  MeasurementModel(this.amount, this.unitLong, this.unitShort);

  MeasurementModel.fromJson(Map<String, dynamic> json)
      : amount = json['amount'],
        unitLong = json['unitLong'],
        unitShort = json['unitShort'];

  Map<String, dynamic> toJson() => {
        'amount': amount,
        'unitLong': unitLong,
        'unitShort': unitShort,
      };
}
