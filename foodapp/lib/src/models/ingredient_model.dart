import 'package:foodapp/src/models/measurement_model.dart';

class IngredientModel extends Object {
  final String aisle;
  final int amount;
  final String consitency;
  final int id;
  final String image;
  final Map<String, MeasurementModel> measures;
  final String meta;
  final String name;
  final String original;
  final String originalName;
  final String unit;

  IngredientModel(
      this.aisle,
      this.amount,
      this.consitency,
      this.id,
      this.image,
      this.measures,
      this.meta,
      this.name,
      this.original,
      this.originalName,
      this.unit);

  IngredientModel.fromJson(Map<String, dynamic> json)
      : aisle = json['aisle'],
        amount = json['amount'],
        consitency = json['consitency'],
        id = json['id'],
        image = json['image'],
        measures = json['measures'],
        meta = json['meta'],
        name = json['name'],
        original = json['original'],
        originalName = json['originalName'],
        unit = json['unit'];

  Map<String, dynamic> toJson() => {
        'aisle': aisle,
        'amount': amount,
        'consitency': consitency,
        'id': id,
        'image': image,
        'measures': measures,
        'meta': meta,
        'name': name,
        'original': original,
        'originalName': originalName,
        'unit': unit,
      };
}
