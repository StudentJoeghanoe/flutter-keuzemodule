class RecipeModel {
  final int id;
  final String title;
  final String image;
  final String imageType;
  final int servings;
  final int readyInMinutes;
  final String sourceName;
  final String sourceUrl;
  final String spoonacularSourceUrl;
  final int aggregateLikes;
  final double healthScore;
  final double spoonacularScore;
  final double pricePerServing;
  final String instructions;
  final List<dynamic> analyzedInstructions;
  final bool cheap;
  final String creditsText;
  final bool dairyFree;
  final List<dynamic> diets;
  final String gaps;
  final bool glutenFree;
  final bool ketogenic;
  final bool lowFodmap;
  final bool sustainable;
  final bool vegan;
  final bool vegetarian;
  final bool veryHealthy;
  final bool veryPopular;
  final bool whole30;
  final int weightWatcherSmartPoints;
  final List<String> dishTypes;
  final List<Map<String, dynamic>> extendedIngredients;
  final String summary;

  RecipeModel(
      this.id,
      this.title,
      this.image,
      this.imageType,
      this.servings,
      this.readyInMinutes,
      this.sourceName,
      this.sourceUrl,
      this.spoonacularSourceUrl,
      this.aggregateLikes,
      this.healthScore,
      this.spoonacularScore,
      this.pricePerServing,
      this.instructions,
      this.analyzedInstructions,
      this.cheap,
      this.creditsText,
      this.dairyFree,
      this.diets,
      this.gaps,
      this.glutenFree,
      this.ketogenic,
      this.lowFodmap,
      this.sustainable,
      this.vegan,
      this.vegetarian,
      this.veryHealthy,
      this.veryPopular,
      this.whole30,
      this.weightWatcherSmartPoints,
      this.dishTypes,
      this.summary,
      this.extendedIngredients);

  RecipeModel.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        image = json['image'],
        id = json['id'],
        imageType = json['imageType'],
        servings = json['servings'],
        readyInMinutes = json['readyInMinutes'],
        sourceName = json['sourceName'],
        sourceUrl = json['sourceUrl'],
        spoonacularSourceUrl = json['spoonacularSourceUrl'],
        aggregateLikes = json['aggregateLikes'],
        healthScore = json['healthScore'],
        spoonacularScore = json['spoonacularScore'],
        pricePerServing = json['pricePerServing'],
        instructions = json['instructions'],
        analyzedInstructions = json['analyzedInstructions'],
        cheap = json['cheap'],
        creditsText = json['creditsText'],
        dairyFree = json['dairyFree'],
        diets = json['diets'],
        gaps = json['gaps'],
        glutenFree = json['glutenFree'],
        ketogenic = json['ketogenic'],
        lowFodmap = json['lowFodmap'],
        sustainable = json['sustainable'],
        vegan = json['vegan'],
        vegetarian = json['vegetarian'],
        veryHealthy = json['veryHealthy'],
        veryPopular = json['veryPopular'],
        whole30 = json['whole30'],
        weightWatcherSmartPoints = json['weightWatcherSmartPoints'],
        dishTypes = json['dishTypes'],
        summary = json['summary'],
        extendedIngredients = json['extendedIngredients'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'image': image,
        'imageType': imageType,
        'servings': servings,
        'readyInMinutes': readyInMinutes,
        'sourceName': sourceName,
        'sourceUrl': sourceUrl,
        'spoonacularSourceUrl': spoonacularSourceUrl,
        'aggregateLikes': aggregateLikes,
        'healthScore': healthScore,
        'spoonacularScore': spoonacularScore,
        'pricePerServing': pricePerServing,
        'instructions': instructions,
        'analyzedInstructions': analyzedInstructions,
        'cheap': cheap,
        'creditsText': creditsText,
        'dairyFree': dairyFree,
        'diets': diets,
        'gaps': gaps,
        'glutenFree': glutenFree,
        'ketogenic': ketogenic,
        'lowFodmap': lowFodmap,
        'sustainable': sustainable,
        'vegan': vegan,
        'vegetarian': vegetarian,
        'veryHealthy': veryHealthy,
        'veryPopular': veryPopular,
        'whole30': whole30,
        'weightWatcherSmartPoints': weightWatcherSmartPoints,
        'dishTypes': dishTypes,
        'summary': summary,
        'extendedIngredients': extendedIngredients
      };
}
